#!/usr/bin/env python

import csv
import matplotlib.pyplot as plt
import colorsys
import argparse


def plot_instances(u_data, smt_data, basic_sym, basic_asym,
                   rel_sym, rel_asym, red_sym, red_asym, X, usmt,
                   inst):
    fig, ax = plt.subplots()
    ax.bar(X, u_data, color='black', label='unsolved')
    ax.bar(X, smt_data, color='green', bottom=u_data,
           label='smt')
    ax.bar(X, [basic_sym], color='red', bottom=usmt,
           label='basic_sym')
    ax.bar(X, [basic_asym], color='maroon',
           bottom=[usmt[0]+basic_sym], label='basic_asym')
    ax.bar(X, [red_sym], color='blue',
           bottom=[usmt[0]+basic_sym+basic_asym],
           label='ilp_red_sym')
    ax.bar(X, [red_asym], color='pink',
           bottom=[usmt[0]+basic_sym+basic_asym+red_sym],
           label='ilp_red_asym')
    ax.bar(X, [rel_sym], color='yellow',
           bottom=[usmt[0]+basic_sym+basic_asym+red_sym+red_asym],
           label='ilp_rel_sym')
    ax.bar(X, [rel_asym], color='gray',
           bottom=[usmt[0] + basic_sym +
                   basic_asym+red_sym+red_asym+rel_sym],
           label='ilp_rel_asym')

    ax.set_ylabel('% contribution by different Techniques')
    ax.set_title('Comparison of different solving techniques')
    ax.set_xlabel(list(inst)[0])
    plt.legend(bbox_to_anchor=(1.1, 1.07), loc=2)
    plt.grid(which='major')
    plt.grid(which='minor')
    plt.savefig(list(inst)[0]+'.pdf', format='pdf', bbox_inches='tight')


# Read the 'CSV' file
def readFile(name):
    milps = []
    smts = []
    with open(name, 'rb') as csvfile:
        reader = csv.DictReader(csvfile,
                                ['Instance', 'Structure',
                                 'Balanced', 'Ignore1',
                                 'Tasks', 'CCR',
                                 'Random', 'Procs',
                                 'Technique', 'Milptech',
                                 'Symmetry', 'Lowerbound',
                                 'Solution', 'Time'])

        # Make a list of dicts
        maxInst = 0
        structures = []
        ccrs = []
        procs = []
        tasks = []
        for r in reader:
            if r['CCR'] == 'None':
                r['CCR'] = str(-1)
            else:
                tmp = str(round(float(r['CCR'])))
                if tmp == '0.0':
                    r['CCR'] = '0.1'
                else:
                    r['CCR'] = str(round(float(r['CCR'])))

            structures += [r['Structure']]
            ccrs += [r['CCR']]
            procs += [r['Procs']]
            tasks += [r['Tasks']]
            maxInst = max(int(r['Instance']), maxInst)
            if r['Technique'] == 'smt':
                smts += [r]
            else:
                milps += [r]

        return (smts, milps, maxInst, list(set(structures)),
                list(set(ccrs)), list(set(procs)),
                list(set(tasks)))


def solved_instances(smts, milps, largestInst, plot, debug=True):
    s_smts = []
    s_basics_asym = []
    s_basics_sym = []
    s_rel_sym = []
    s_rel_asym = []
    s_red_sym = []
    s_red_asym = []
    s_unsolved = []
    for i in xrange(largestInst+1):
        inst = set([x['Structure']+'_CCR_'+x['CCR'] +
                    '_PROCS_' + x['Procs'] + '_#TASKS_' +
                    x['Tasks'] + '_INST_' +
                    x['Instance']
                    for x in smts
                    if int(x['Instance']) == i])
        solved_smts = [x
                       for x in smts
                       if float(x['Time']) < 60 and
                       int(x['Instance']) == i]
        s_smts += solved_smts
        solved_basics_sym = [x for x in milps
                             if (x['Milptech'] == 'BASIC' and
                                 float(x['Time']) < 60 and
                                 x['Symmetry'] == 'no antisym' and
                                 int(x['Instance']) == i)]
        s_basics_sym += solved_basics_sym
        solved_basics_asym = [x for x in milps
                              if (x['Milptech'] == 'BASIC' and
                                  float(x['Time']) < 60 and
                                  x['Symmetry'] == 'antisym' and
                                  int(x['Instance']) == i)]
        s_basics_asym += solved_basics_asym
        solved_relaxed_sym = [x for x in milps
                              if (x['Milptech'] == 'RELAXED' and
                                  float(x['Time']) < 60 and
                                  x['Symmetry'] == 'no antisym'and
                                  int(x['Instance']) == i)]
        s_rel_sym += solved_relaxed_sym
        solved_relaxed_asym = [x for x in milps
                               if (x['Milptech'] == 'RELAXED' and
                                   float(x['Time']) < 60 and
                                   x['Symmetry'] == 'antisym' and
                                   int(x['Instance']) == i)]
        s_rel_asym += solved_relaxed_asym
        solved_reduced_sym = [x for x in milps
                              if (x['Milptech'] == 'REDUCED' and
                                  float(x['Time']) < 60 and
                                  x['Symmetry'] == 'no antisym' and
                                  int(x['Instance']) == i)]
        s_red_sym += solved_reduced_sym
        solved_reduced_asym = [x for x in milps
                               if (x['Milptech'] == 'REDUCED' and
                                   float(x['Time']) < 60 and
                                   x['Symmetry'] == 'antisym' and
                                   int(x['Instance']) == i)]
        s_red_asym += solved_reduced_asym
        unsolved1 = [x
                     for x in smts
                     if float(x['Time']) >= 60 and
                     int(x['Instance']) == i]
        unsolved2 = [x
                     for x in milps
                     if float(x['Time']) >= 60 and
                     int(x['Instance']) == i]
        unsolved = unsolved1 + unsolved2
        s_unsolved += unsolved

        total1 = [x for x in smts if int(x['Instance']) == i]
        total2 = [x for x in milps if int(x['Instance']) == i]
        total = total1 + total2
        tot = float(len(total))

        X = [x for x in xrange(1)]

        u_data = [len(unsolved)/tot*100.0 for x in xrange(1)]
        smt_data = [len(solved_smts)/tot*100.0 for x in xrange(1)]
        usmt = [smt_data[i]+u_data[i] for i in xrange(1)]

        basic_sym = len(solved_basics_sym)/tot*100.0
        basic_asym = len(solved_basics_asym)/tot*100.0
        red_sym = len(solved_reduced_sym)/tot*100.0
        red_asym = len(solved_reduced_asym)/tot*100.0
        rel_sym = len(solved_relaxed_sym)/tot*100.0
        rel_asym = len(solved_relaxed_asym)/tot*100.0

        if plot:
            plot_instances(u_data, smt_data, basic_sym, basic_asym,
                           rel_sym, rel_asym, red_sym, red_asym, X,
                           usmt, inst)

        if debug:
            print "Instance: ", list(inst)[0]
            if (len(solved_smts) < len(solved_basics_sym) or
                len(solved_smts) < len(solved_basics_asym) or
                len(solved_smts) < len(solved_reduced_sym) or
                len(solved_smts) < len(solved_reduced_asym)or
                len(solved_smts) < len(solved_relaxed_sym) or
                len(solved_smts) < len(solved_relaxed_asym)):
                print "CHECK"
            print "solved_smts: ", len(solved_smts)
            print "solved_basics_sym: ", len(solved_basics_sym)
            print "solved_relaxed_sym: ", len(solved_relaxed_sym)
            print "solved_reduced_sym: ", len(solved_reduced_sym)
            print "solved_basics_asym: ", len(solved_basics_asym)
            print "solved_relaxed_asym: ", len(solved_relaxed_asym)
            print "solved_reduced_asym: ", len(solved_reduced_asym)
            print "unsolved: ", len(unsolved)
            print "total: ", len(total)
            print "--------------", "\n\n\n"

    return (s_smts, s_basics_sym, s_basics_asym,
            s_rel_sym, s_rel_asym, s_red_sym,
            s_red_asym, s_unsolved)


def plot(x, ys, title, ylabel, xlabel, xtick_labels):
    fig, ax = plt.subplots()
    bottom = [0.0]*len(x)
    # Generate some colors
    HSV_tuples = [(c*1.0/(len(ys.keys())), 0.3, 0.9)
                  for c in range(len(ys.keys()))]
    RGB_tuples = map(lambda r: colorsys.hsv_to_rgb(*r), HSV_tuples)
    for i, (k, v) in zip(range(len(ys.keys())), list(ys.iteritems())):
        ax.bar(x, v, color=RGB_tuples[i], bottom=bottom, label=k)
        # Now reduce the bottom
        bottom = [b+y for b, y in zip(bottom, v)]

    ax.set_ylabel(ylabel)
    ax.set_xlabel(xlabel)
    ax.set_title(title)
    # plt.legend(tuple(graphs), tuple(legend), bbox_to_anchor=(1.5, 2))
    plt.legend(bbox_to_anchor=(1.1, 1.07), loc=2)
    plt.xticks(x, xtick_labels, rotation='vertical')
    plt.minorticks_on()
    plt.grid(which='major')
    plt.grid(which='minor')
    # plt.show()
    plt.savefig(title+'.pdf', format='pdf', bbox_inches='tight')


# Plot comparison of same structures from amongst the solved instances.
def plot_struct(s_smts, s_basics_sym, s_basics_asym,
                s_rel_sym, s_rel_asym, s_red_sym,
                s_red_asym, s_unsolved, kw, iterator,
                xlabel):

    ss_smt = []
    ss_b_s = []
    ss_b_as = []
    ss_rel_s = []
    ss_rel_as = []
    ss_red_s = []
    ss_red_as = []
    ss_u = []
    ss_tot = []

    for s in iterator:
        smt = len([x for x in s_smts if x[kw] == s])
        ss_smt += [smt]
        b_sym = len([x for x in s_basics_sym if x[kw] == s])
        ss_b_s += [b_sym]
        b_asym = len([x for x in s_basics_asym if x[kw] == s])
        ss_b_as += [b_asym]
        rel_sym = len([x for x in s_rel_sym if x[kw] == s])
        ss_rel_s += [rel_sym]
        rel_asym = len([x for x in s_rel_asym if x[kw] == s])
        ss_rel_as += [rel_asym]
        red_sym = len([x for x in s_red_sym if x[kw] == s])
        ss_red_s += [red_sym]
        red_asym = len([x for x in s_red_sym if x[kw] == s])
        ss_red_as += [red_asym]
        unsolved = len([x for x in s_unsolved if x[kw] == s])
        ss_u += [unsolved]

        total = (smt + b_sym + b_asym +
                 rel_sym + rel_asym + red_sym + red_asym + unsolved)
        # total = len([x for x in (smts + milps) if x['Structure'] == s])
        ss_tot += [float(total)]

    # The x-coordinates
    X = [x for x in xrange(len(iterator))]
    # The Y coordinates
    Y_smt = [x/y*100.0 for x, y in zip(ss_smt, ss_tot)]
    Y_b_s = [x/y*100.0 for x, y in zip(ss_b_s, ss_tot)]
    Y_b_as = [x/y*100.0 for x, y in zip(ss_b_as, ss_tot)]
    Y_rel_s = [x/y*100.0 for x, y in zip(ss_rel_s, ss_tot)]
    Y_rel_as = [x/y*100.0 for x, y in zip(ss_rel_as, ss_tot)]
    Y_red_s = [x/y*100.0 for x, y in zip(ss_red_s, ss_tot)]
    Y_red_as = [x/y*100.0 for x, y in zip(ss_red_as, ss_tot)]
    Y_u = [x/y*100.0 for x, y in zip(ss_u, ss_tot)]
    # The dictionary to plot
    ys = {'smt': Y_smt, 'ilp_basic_sym': Y_b_s,
          'ilp_basic_asym': Y_b_as, 'ilp_relaxed_sym': Y_rel_s,
          ' ilp_relaxed_asym': Y_rel_as, 'ilp_reduced_sym': Y_red_s,
          'ilp_reduced_asym': Y_red_as, 'unsolved': Y_u}
    plot(X, ys,
         'Comparison of different techniques for different' + xlabel,
         '% contribution of different techniques',
         xlabel, iterator)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("file")
    parser.add_argument("-t", "--task", help="Print the tasks chart",
                        action="store_true")
    parser.add_argument("-p", "--proc", help="Print the procs chart",
                        action="store_true")
    parser.add_argument("-c", "--ccr", help="Print the ccr chart",
                        action="store_true")
    parser.add_argument("-s", "--struct", help="Print the struct chart",
                        action="store_true")
    parser.add_argument("--plot", help="Print *all* the instance charts",
                        action="store_true")

    args = parser.parse_args()

    (smts, milps, largestInst,
     structs, ccrs, procs, tasks) = readFile(args.file)

    # Plotting the individual instances separately
    (s_smts, s_basics_sym, s_basics_sym,
     s_rel_sym, s_rel_sym, s_red_sym, s_red_sym,
     s_unsolved) = solved_instances(smts, milps, largestInst, args.plot,
                                    debug=False)

    if args.struct:
        plot_struct(s_smts, s_basics_sym, s_basics_sym,
                    s_rel_sym, s_rel_sym, s_red_sym, s_red_sym,
                    s_unsolved, 'Structure', structs, ' Task structures')

    if args.ccr:
        plot_struct(s_smts, s_basics_sym, s_basics_sym,
                    s_rel_sym, s_rel_sym, s_red_sym, s_red_sym,
                    s_unsolved, 'CCR', ccrs, ' CCR')

    if args.proc:
        plot_struct(s_smts, s_basics_sym, s_basics_sym,
                    s_rel_sym, s_rel_sym, s_red_sym, s_red_sym,
                    s_unsolved, 'Procs', procs, ' # of Procs')

    if args.task:
        plot_struct(s_smts, s_basics_sym, s_basics_sym,
                    s_rel_sym, s_rel_sym, s_red_sym, s_red_sym,
                    s_unsolved, 'Tasks', tasks, ' # of Tasks')
